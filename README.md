# emacs docker image

This is a test docker image including a set of tools used for development:

- emacs
- doom-emacs (an emacs configuration framework)
- a number of emacs packages specific to my configuration

the image was created using:

```sh
docker save xeijin/emacs-test | gzip > emacs.tar.gz
```

The original docker image can be found at:

https://hub.docker.com/r/xeijin/emacs-test